export class SwimStyle {
  id: number;
  name: string;
  v1: number;
  v2: number;
  v3: number;

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
    this.v1 = null;
    this.v2 = null;
    this.v3 = null;
  }
}
