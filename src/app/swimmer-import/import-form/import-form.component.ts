import { Component, OnInit } from '@angular/core';
import { ImportService } from './import.service';
import { SwimmerRecord } from '../swimmer.model';

@Component({
  selector: 'app-import-form',
  templateUrl: './import-form.component.html',
  styleUrls: ['./import-form.component.css']
})
export class ImportFormComponent implements OnInit {
  constructor(public importService: ImportService) {}

  ngOnInit() {}

  onClickNew(): void {
    this.importService.insertNew(this.importService.selectedSwimmer);
  }

  onClickUpdate(): void {
    this.importService.update(this.importService.selectedSwimmer);
  }

  onClickDelete(): void {
    const key = this.importService.selectedSwimmer.$key;
    if (key) {
      this.importService.deleteSwimmerRecord(key);
    }
  }

  onClickClear(): void {
    this.importService.clear();
  }

  onCompChange($event): void {
    this.importService.selectedCompatition = this.importService.availableCompatitionRecords[$event.value];
    this.importService.selectedCompId = $event.value;
  }
}
