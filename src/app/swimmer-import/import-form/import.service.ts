import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { SwimmerRecord, CompatitionRecord } from '../swimmer.model';
import { SwimStyle } from './swim-style.model';

@Injectable()
export class ImportService {
  swimmerList: AngularFireList<any>;
  selectedSwimmer: SwimmerRecord = new SwimmerRecord();
  swimStyles: Array<SwimStyle>;
  availableCompatitionRecords: Array<CompatitionRecord>;
  selectedCompatition: CompatitionRecord;
  selectedCompId: number = 0;

  constructor(private firebase: AngularFireDatabase) {
    this.createEmptyStyles();
    this.createEmptyCompatitionRecords();
  }

  getData() {
    this.swimmerList = this.firebase.list<any>('/');
    return this.swimmerList;
  }

  insertNew(record: SwimmerRecord): void {
    if (!this.validRecord(record)) {
      alert('**Kurčina**: Required fileds are empty.');
      return;
    }
    this.fillCompatitionForSelectedSwimmer();
    this.swimmerList.push({
      swimmerCountry: record.swimmerCountry,
      swimmerName: record.swimmerName,
      swimmerPhotoURL: record.swimmerPhotoURL,
      swimmerGender: record.swimmerGender,
      swimmerStrokesAndCoefs: this.fromSwimStylesToAny(this.swimStyles),
      compatitionRecords: record.compatitionRecords
    });
  }

  update(record: SwimmerRecord): void {
    if (!this.validRecord(record)) {
      alert('**Kurčina**: Required fileds are empty.');
      return;
    }
    this.fillCompatitionForSelectedSwimmer();
    this.swimmerList.update(record.$key, {
      swimmerCountry: record.swimmerCountry,
      swimmerName: record.swimmerName,
      swimmerPhotoURL: record.swimmerPhotoURL,
      swimmerGender: record.swimmerGender,
      swimmerStrokesAndCoefs: this.fromSwimStylesToAny(this.swimStyles),
      compatitionRecords: record.compatitionRecords
    });
  }

  deleteSwimmerRecord(key: string) {
    if (confirm('Are you sure to delete this record ?') === true) {
      this.swimmerList.remove(key);
    }
  }

  createEmptyStyles(): void {
    this.swimStyles = [];
    this.swimStyles.push(new SwimStyle(0, 'Butterfly'));
    this.swimStyles.push(new SwimStyle(1, 'Backstroke'));
    this.swimStyles.push(new SwimStyle(2, 'Breaststroke'));
    this.swimStyles.push(new SwimStyle(3, 'Freestyle'));
  }

  createEmptyCompatitionRecords(): void {
    this.availableCompatitionRecords = [];
    this.availableCompatitionRecords.push({ compatitionId: 0, compatitionName: 'World', gold: 0, silver: 0, bronze: 0 });
    this.availableCompatitionRecords.push({ compatitionId: 1, compatitionName: 'Olimpics', gold: 0, silver: 0, bronze: 0 });
    this.availableCompatitionRecords.push({ compatitionId: 2, compatitionName: 'NCAA', gold: 0, silver: 0, bronze: 0 });
    this.selectedCompatition = this.availableCompatitionRecords[this.selectedCompId];
  }

  fillCompatitionForSelectedSwimmer(): void {
    this.selectedSwimmer.compatitionRecords = [];
    for (const comp of this.availableCompatitionRecords) {
      this.selectedSwimmer.compatitionRecords.push(comp);
    }
  }

  clear(): void {
    this.selectedSwimmer = new SwimmerRecord();
    this.createEmptyStyles();
  }

  createStylesFromSelectedSwimmer(): void {
    this.swimStyles = [];
    if (!this.selectedSwimmer.swimmerStrokesAndCoefs) {
      this.createEmptyStyles();
      return;
    }
    const s0 = this.selectedSwimmer.swimmerStrokesAndCoefs[0];
    const s1 = this.selectedSwimmer.swimmerStrokesAndCoefs[1];
    const s2 = this.selectedSwimmer.swimmerStrokesAndCoefs[2];
    const s3 = this.selectedSwimmer.swimmerStrokesAndCoefs[3];

    if (s0) {
      this.swimStyles.push(this.fillStrokesForId(0, s0));
    } else {
      this.swimStyles.push(new SwimStyle(0, 'Butterfly'));
    }

    if (s1) {
      this.swimStyles.push(this.fillStrokesForId(1, s1));
    } else {
      this.swimStyles.push(new SwimStyle(1, 'Backstroke'));
    }

    if (s2) {
      this.swimStyles.push(this.fillStrokesForId(2, s2));
    } else {
      this.swimStyles.push(new SwimStyle(2, 'Breaststroke'));
    }

    if (s3) {
      this.swimStyles.push(this.fillStrokesForId(3, s3));
    } else {
      this.swimStyles.push(new SwimStyle(3, 'Freestyle'));
    }
  }

  createCurrentCompatitionFromSelectedSwimmer(): void {
    this.createEmptyCompatitionRecords();
    if (!this.selectedSwimmer.compatitionRecords) {
      return;
    }
    for (const prop in this.selectedSwimmer.compatitionRecords) {
      if (this.selectedSwimmer.compatitionRecords.hasOwnProperty(prop)) {
        const i: number = Number(prop);
        this.availableCompatitionRecords[i].gold = this.selectedSwimmer.compatitionRecords[i].gold;
        this.availableCompatitionRecords[i].silver = this.selectedSwimmer.compatitionRecords[i].silver;
        this.availableCompatitionRecords[i].bronze = this.selectedSwimmer.compatitionRecords[i].bronze;
      }
    }
  }

  fillStrokesForId(id: number, strokeValues: any): SwimStyle {
    let style: SwimStyle;
    if (id === 0) {
      style = new SwimStyle(id, 'Butterfly');
    } else if (id === 1) {
      style = new SwimStyle(id, 'Backstroke');
    } else if (id === 2) {
      style = new SwimStyle(id, 'Breaststroke');
    } else if (id === 3) {
      style = new SwimStyle(id, 'Freestyle');
    }
    style.v1 = strokeValues[0];
    style.v2 = strokeValues[1];
    style.v3 = strokeValues[2];
    return style;
  }

  fromSwimStylesToAny(styles: Array<SwimStyle>) {
    const obj = {};
    obj[0] = {};
    obj[0][0] = styles[0].v1 ? styles[0].v1 : null;
    obj[0][1] = styles[0].v2 ? styles[0].v2 : null;
    obj[0][2] = styles[0].v3 ? styles[0].v3 : null;

    obj[1] = {};
    obj[1][0] = styles[1].v1 ? styles[1].v1 : null;
    obj[1][1] = styles[1].v2 ? styles[1].v2 : null;
    obj[1][2] = styles[1].v3 ? styles[1].v3 : null;

    obj[2] = {};
    obj[2][0] = styles[2].v1 ? styles[2].v1 : null;
    obj[2][1] = styles[2].v2 ? styles[2].v2 : null;
    obj[2][2] = styles[2].v3 ? styles[2].v3 : null;

    obj[3] = {};
    obj[3][0] = styles[3].v1 ? styles[3].v1 : null;
    obj[3][1] = styles[3].v2 ? styles[3].v2 : null;
    obj[3][2] = styles[3].v3 ? styles[3].v3 : null;

    return obj;
  }

  validRecord(record: SwimmerRecord): boolean {
    let b = record.swimmerCountry && record.swimmerCountry.length > 0;
    b = b && record.swimmerName && record.swimmerName.length > 0;
    b = b && record.swimmerGender && record.swimmerGender.length > 0;
    b = b && this.swimStyles.length > 0;
    return b;
  }
}
