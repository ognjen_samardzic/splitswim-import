export class SwimmerRecord {
  $key: string;
  swimmerCountry: string;
  swimmerName: string;
  swimmerPhotoURL: string;
  swimmerGender: string;
  swimmerStrokesAndCoefs: any;
  compatitionRecords: Array<CompatitionRecord>;

  constructor() {
    this.swimmerCountry = '';
    this.swimmerName = '';
    this.swimmerPhotoURL = '';
  }
}

export interface CompatitionRecord {
  compatitionId: number;
  compatitionName: string;
  gold: number;
  silver: number;
  bronze: number;
}
