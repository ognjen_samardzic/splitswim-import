import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwimmerImportComponent } from './swimmer-import.component';
import { ImportService } from './import-form/import.service';
import { ImportFormComponent } from './import-form/import-form.component';
import { SwimmerTableComponent } from './swimmer-table/swimmer-table.component';
import {MatListModule, MatGridListModule, MatButtonModule, MatCardModule, MatInputModule, MatSelectModule} from '@angular/material';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    SwimmerImportComponent,
    ImportFormComponent,
    SwimmerTableComponent
],
  imports: [
    CommonModule,
    FormsModule,
    MatListModule,
    MatGridListModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule
  ],
  exports: [SwimmerImportComponent],
  providers: [ImportService]
})
export class SwimmerImportModule { }
