import { Component, OnInit, ViewChild } from '@angular/core';
import { ImportService } from '../import-form/import.service';
import { SwimmerRecord } from '../swimmer.model';
import { MatSelectionList, MatListOption } from '@angular/material';

@Component({
  selector: 'app-swimmer-table',
  templateUrl: './swimmer-table.component.html',
  styleUrls: ['./swimmer-table.component.css']
})
export class SwimmerTableComponent implements OnInit {
  fullRecordList: Array<SwimmerRecord>;
  swimmerRecordList: Array<SwimmerRecord>;
  nameFilter: string = '';
  genderFilter: string = '0';

  @ViewChild('selectionList') selectionList: MatSelectionList;

  constructor(private importService: ImportService) {}

  ngOnInit() {
    const x = this.importService.getData();
    x.snapshotChanges().subscribe(item => {
      this.swimmerRecordList = [];
      this.fullRecordList = [];
      item.forEach(element => {
        const y = element.payload.toJSON();
        y['$key'] = element.key;
        const rec = y as SwimmerRecord;
        this.swimmerRecordList.push(y as SwimmerRecord);
        if (rec.swimmerName === 'Testo Testic') {
          console.log('asfdg');
        }
        this.fullRecordList.push(y as SwimmerRecord);
      });
    });
  }

  onItemClick(swimRec: SwimmerRecord, listOption: MatListOption): void {
    this.importService.selectedSwimmer = Object.assign({}, swimRec);
    this.selectionList.deselectAll();
    listOption.selected = true;
    this.importService.createStylesFromSelectedSwimmer();
    this.importService.createCurrentCompatitionFromSelectedSwimmer();
  }

  nameFilterInputKeyup($event): void {
    if ($event.keyCode === 13) {
      this.onNameFilterChange();
    } else if ($event.keyCode === 27) {
      this.nameFilter = '';
      this.onNameFilterChange();
    }
  }

  onNameFilterChange(): void {
    this.nameFilter = this.nameFilter.toLowerCase().trim();
    if (this.nameFilter === '') {
      this.swimmerRecordList = this.fullRecordList;
      return;
    }

    this.swimmerRecordList = this.swimmerRecordList.filter(item => {
      return item.swimmerName.toLowerCase().indexOf(this.nameFilter) > -1;
    });
  }

  genderFilterSelectChange($event): void {
    if ($event.value === '0') {
      this.swimmerRecordList = this.fullRecordList;
    } else {
      this.swimmerRecordList = this.fullRecordList.filter(item => {
        return item.swimmerGender && item.swimmerGender === $event.value;
      });
    }
  }
}
