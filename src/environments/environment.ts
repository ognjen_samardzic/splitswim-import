// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCK8zhkyV799F1dgereRgS8NbqfkC5_oiI',
    authDomain: 'splitswim.firebaseapp.com',
    databaseURL: 'https://splitswim.firebaseio.com',
    projectId: 'splitswim',
    storageBucket: 'splitswim.appspot.com',
    messagingSenderId: '675893033041'
  }
};
