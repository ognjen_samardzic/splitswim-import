# SplitSwimImport
This is simple Firebase + Angular 4+ CRUD app.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.0.
Firebase database [link](https://console.firebase.google.com/project/splitswim/overview).
# Electron
This [tutorial](https://angularfirebase.com/lessons/desktop-apps-with-electron-and-angular/) is used for electron.

# Build application
1. Run `npm install`, if you don't have Angular CLI install it.
2. Run `ng build --prod`
3. Install electron-packager: 
  `npm install electron-packager -g`
  `npm install electron-packager --save-dev`
4. Build:
  Windows: `electron-packager . --platform=win32`
  MacOS: `electron-packager . --platform=darwin`

